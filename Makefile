all: client server
client:
	gcc -o client udpClient.c -lpthread
server:
	gcc -o server udpServer.c -lpthread

clean:
	rm client server data*

update:
	cp /mnt/hgfs/data/custom_udp4-malloc/udpClient.c ./
	cp /mnt/hgfs/data/custom_udp4-malloc/udpServer.c ./